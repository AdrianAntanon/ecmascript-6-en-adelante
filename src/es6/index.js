/* Clase 1 */

// Antes de EC 6+
function newFunction(name, age, country){
    var name = name || 'adrián';
    var age = age || 26;
    var country = country || 'ES';
    console.log(name, age, country)
}

// Con EC 6+
function newFunction2(name='Adrián', age=26,country='ES'){
    console.log(name, age, country)
}

newFunction2()
newFunction2('Joel',20,'MX')


let hello = 'Hello'
let world = 'World'
let epicPhrase = hello + ' ' + world
console.log(epicPhrase)

let epicPhrase2 = `${hello} ${world}`

console.log(epicPhrase2)

/* Clase 2 */

// Antes de EC 6+

let lorem = "Lorem ipsum dolor sit \n" 
+"amet consectetur adipisicing elit. Earum, ex."

// Con EC 6+

let lorem2= `Otra frase épica que 
quiero usar`

console.log(lorem)
console.log(lorem2)


let person = {
    'name': 'Adri',
    'age': 26,
    'country': 'ES'
}

console.log(person.name, person.age)

let {name, age, country} = person

console.log(name, age, country)


let team1 = ['Adri','Joel','Alex']
let team2 = ['Javi', 'Andrea', 'David']

let education = ['David', ...team1, ...team2]

// Al poner ...team1 lo que estoy haciendo es desglosar el contenido de team1 y añadirlo a la nueva variable

console.log(education)


// Antes ES 6
{
    var globalVar = "Global Var";
}

// Con ES 6
{
    let globalLet = 'Global Let'
    console.log(globalLet)
}

console.log(globalVar)
// console.log(globalLet) NO está definida fuera de su bloque original, por lo que es mejor, ya que no te pisarás variables si no quieres


const a = 'b'
a = 'b'
// Esto falla porque evidentemente, una constante solo se puede definir en su creación, no se puede modificar

/* Clase 3 */

// Arrow Functions, Promesas y Parámetros en objetos

// Antes EC 6+

let name = 'adri'
let age = 26

obj = { name: name, age: age}

// Con EC 6

obj2 = {name, age}

console.log(obj2)

const names = [
    {name: 'Adrián', age: 26},
    {name: 'Andrea', age: 25}
]


// Antes EC 6+

let listOfNames = names.map(function(item){
    console.log(item.name)
})

// Con EC 6

let listOfNames2 = names.map(item => console.log(item.name))

/* const listOfNames3 = (name, age, country) => {
    ...
} */

const listOfNames4 = name => {
    console.log('...')
}

const square = num => num*num


// Promesa, una promesa SIEMPRE se declara así, y lleva
// dos funciones por defecto, una por si se cumple y otra por si falla
// De la siguiente forma quedaría: 

const helloPromise = () => {
    return new Promise((resolve, reject) => {
        if(true){ // si hago un if false salta al else y deja de ejecutar los .then para pasar al catch
            resolve('Guay')
        }else{
            reject('No tan guay')
        }
    })
}

// Esto es como un java, cuando invoco una promesa he de hacerlo con un try catch, que en JS es .then .catch.
// Además puedo tener muchos then si es necesario

helloPromise()
    .then(response => console.log(response))
    .then(() => console.log('Hi, im a promise'))
    .catch(error => console.log(error))

/* Clase 4 */

// Clases, módulos y generadores

class calculator{
    constructor(){
        this.valueA = 0
        this.valueB = 0
    }

    sum(valueA, valueB){
        this.valueA = valueA
        this.valueB = valueB
        return this.valueA + this.valueB
    }
}

const calc = new calculator()
console.log(calc.sum(2,2))

// Import y Export(está en el archivo module.js)

import{ hello } from './module'

hello()

// Generadores

function* helloWorld(){
    if(true){
        yield 'Hello, '
    }
    if(true){
        yield 'World'
    }

    yield 0
    yield 2
    yield 3
    yield 4
}

const generatorHello = helloWorld()
console.log(generatorHello.next().value)
console.log(generatorHello.next().value)
console.log(generatorHello.next().value)


