/* Clase 5 */

// ES 7
// includes 

let numbers = [1, 2, 4, 5, 6]
const searchNumber = 6

if(numbers.includes(searchNumber)){
    console.log(`Sí se encuentra el número ${searchNumber}`)
}else{
    console.log(`No se encuentra el número ${searchNumber}`)
}

// Elevar un número (4^3, por ejemplo) es con **, es decir 4**3
let base = 4
let exponent = 3
let result = base**exponent

console.log(result)