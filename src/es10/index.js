/* Clase 8 */

// ES 10

let array = [1, 2, 3, [1, 2, 3,[1,2,3]]]
// El método flat() crea una nueva matriz con todos los elementos de sub-array concatenados recursivamente hasta la profundidad especificada.
// El nivel de profundidad que especifica qué tan profunda debe aplanarse una estructura de matriz anidada. El valor predeterminado es 1.
console.log(array.flat(2))

let array = [1,2,3,4,5]

console.log(array.flatMap(value => [value, value*2]))

// El método flatMap() primero mapea cada elemento usando una función de mapeo, luego aplana el resultado en una nueva matriz. Es idéntico a un map seguido de un flatten de profundidad 1, pero flatMap es a menudo útil y la fusión de ambos en un método es ligeramente más eficiente.

/* Aquí lo que hace es function(value){
    return value, value*2
} 
es decir, si lo primero que tengo es 1 me devuelve 1 y 1*2, si tengo 2 devuelve 2 y 2*2 y así consecutivamente
*/

let hello = '     hello world'

console.log(hello)
console.log(hello.trimStart())

hello = 'hello world      '
console.log(hello)
console.log(hello.trimEnd())

// La función trimStart/End() sirve para quitar los espacios en blanco de un texto

try{

}catch{ // ya no hace falta especificar el parámetro que recibe, que será (error) en el catch, se sobreentiende que es lo que estamos llamando dentro
    error
}


// Transformar clave-valor en objeto

let entries = [['name','adri'], ['age', 26]]

console.log(Object.fromEntries(entries))
// El método Object.fromEntries() transforma una lista de pares con [clave-valor] en un objeto.


// Objeto de tipo símbolo
let mySymbl = `My Symbol`
let symbol = Symbol(mySymbl)
console.log(symbol.description)