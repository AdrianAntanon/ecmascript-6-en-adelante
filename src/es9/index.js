/* Clase 7 */

// ES 9
const obj = {
    name: 'Adri',
    age: 26,
    country: 'ES'
}

let {country, ...all} = obj

console.log(all)


const obj1 = {
    name: 'adri',
    age: 26,
}
// anidar elementos
const obj2 = {
    ...obj1,
    country: 'ES',
}

console.log(obj2)



const helloWorld = () =>{
    return new Promise((resolve, reject) => {
        (true) //Esto es un if, va implícito en la declaración por lo que no es necesario poner IF delante
            ? setTimeout(() => {
                resolve('Hello World')
            },3000) //si es verdadero se ejecuta este bloque
            : reject(new Error('Test Error')) // y al poner : es el else de toda la vida
    })
}

helloWorld()
    .then(response => console.log(response))
    .catch(error => console.log(error))// Antes de ES 9 con esto ya estaríamos
    .finally(()=>console.log('Finalizó')) //Esto se realiza cuando se haya ejecutado todo, independientemente de que se capture el catch

// Agrupar bloques de regex

const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/
const match = regexData.exec('2018-04-20')
// Explico el trabalenguas, sirve para crear fechas, en el primero(decimos del[0 al 9] en cada posición de {máximo 4} - y así seguimos, resumen [numero que queremos]{cantidad de números})
const year = match[1]
const month = match[2]
const day = match[3]

console.log(year,month,day)