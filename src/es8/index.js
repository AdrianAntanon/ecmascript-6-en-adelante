/* Clase 6 */

// ES 8

const data = {
    frontend: 'Adri',
    backend: 'Andrea',
    design: 'Joel',
}

// entries transforma un objeto en una matriz
const entries = Object.entries(data)
console.log(entries)
console.log(entries.length)

// values coge solo los valores, pasándolos a una array
const values = Object.values(data)
console.log(values)


// padStart y padEnd reciben dos parámetros, la extensión que nos gustaría que tuviesen
// y que queremos añadir en ese espacio nuevo, en caso de que no se rellenen se le asignan guiones

const string = 'hello'

console.log(string.padStart(7, 'hi'))
console.log(string.padEnd(12, ' -----'))
console.log('food'.padEnd(12, ' -----'))


// Async y Await

const helloWorld = () => {
    return new Promise((resolve, reject) => {
        if(true){
            setTimeout(() => resolve('Hello world!'),3000)
        }else{
            reject(new Error('Test error'))
        }
    })
}
// todo lo que que es async tenemos que especificarlo antes
const helloAsync = async() => {
    const hello = await helloWorld()
    console.log(hello)
}

helloAsync()


const anotherFunction = async() => {
    try {
        const hello = await helloWorld()
        console.log(hello)
    } catch (error) {
        console.log(error)        
    }
}

anotherFunction()